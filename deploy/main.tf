terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-angelo"
    key            = "recipe-app.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }

}

provider "aws" {
  region  = "ap-southeast-1"
  version = "~> 3.24.1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}